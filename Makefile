.PHONY: all rebuild clean mrproper install

all: regartercept gm_
rebuild: mrproper all

install:
	cp regartercept /usr/local/sbin/regartercept
	cp gm_ /usr/share/munin/plugins/gm_
	service munin-node restart
	-mkdir /var/log/regartercept/
	chown regartercept /var/log/regartercept/
	@echo "*** Installation done !"

regartercept: regartercept.o crc.o
	@echo "*** Linking regartercept..."
	@gcc regartercept.o crc.o -o regartercept

crc.o: crc.c crc.h
	@echo "*** Compiling crc.o"
	@gcc -Wall -c crc.c -o crc.o

regartercept.o: regartercept.c config.h
	@echo "*** Compiling regartercept.o"
	@gcc -Wall -c regartercept.c -o regartercept.o

gm_: gm_.o
	@echo "*** Linking gm_ (munin plugin)..."
	@gcc gm_.o -o gm_

gm_.o: gm_.c config.h
	@echo "*** Compiling gm_.o"
	@gcc -Wall -c gm_.c -o gm_.o

clean:
	@echo "*** Erasing objects files..."
	@rm -f *.o

mrproper: clean
	@echo "*** Erasing executables..."
	@rm -f regartercept gm_
