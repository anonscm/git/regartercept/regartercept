/*
	REGARTERCEPT
	Copyright CNRS UMR 8181 (2013)

	romain.jooris@univ-lille.fr

	This software is a computer program whose purpose is to passively get
	the data from the RS485 link between a Draeger Regard gases alarm panel
	and its monitoring system.

	This software is governed by the CeCILL license under French law and
	abiding by the rules of distribution of free software.  You can  use,
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info/".

	As a counterpart to the access to the source code and rights to copy,
	modify and redistribute granted by the license, users are provided only
	with a limited warranty  and the software's author, the holder of the
	economic rights,  and the successive licensors  have only limited
	liability.

	In this respect, the user's attention is drawn to the risks associated
	with loading,  using,  modifying and/or developing or reproducing the
	software by the user in light of its specific status of free software,
	that may mean  that it is complicated to manipulate,  and  that  also
	therefore means  that it is reserved for developers  and  experienced
	professionals having in-depth computer knowledge. Users are therefore
	encouraged to load and test the software's suitability as regards their
	requirements in conditions enabling the security of their systems and/or
	data to be ensured and,  more generally, to use and operate it in the
	same conditions as regards security.

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms.
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <termios.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>

#include "crc.h"
#include "config.h"

int daemon_mode = 0;
int file_number_offset = 0;
void print_usage(void) {
	fprintf(stderr, "Usage: regartercept [options] <serial device> <Channels.csv file from Draeger UTF-8 encoded> [logfile] [output dir]\n\n");
	fprintf(stderr, "Options:\n");
	fprintf(stderr, "  -d           Daemon mode (less informations to terminal)\n");
	fprintf(stderr, "  -n N         Offset the output filenames numbers to +N channels\n\n");
}

const char *logfile = NULL;
int printlog(int errno_, const char *msg1, const char *msg2) {
	FILE *fd;
	struct tm *ptm;
	time_t lt;

	lt = time(NULL);
	ptm = localtime(&lt);

	if (!daemon_mode) {
		fprintf(stderr, "[%04u-%02u-%02u %02u:%02u:%02u] ", ptm->tm_year+1900, ptm->tm_mon+1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
	}
	if (msg1)   { fprintf(stderr, "%s", msg1); }
	if (msg2)   { fprintf(stderr, " %s", msg2); }
	if (errno_) { fprintf(stderr, ": %s", strerror(errno_)); }
	fprintf(stderr, "\n");

	fd = fopen(logfile, "a");
	if (!fd) { // log not ok
		perror(logfile);
		return -1;
	}
	else {
		fprintf(fd, "[%04u-%02u-%02u %02u:%02u:%02u] ", ptm->tm_year+1900, ptm->tm_mon+1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
		if (msg1)   { fprintf(fd, "%s", msg1); }
		if (msg2)   { fprintf(fd, " %s", msg2); }
		if (errno_) { fprintf(fd, ": %s", strerror(errno_)); }
		fprintf(fd, "\n");
		if (fclose(fd)) {
			perror("fclose()");
			return -2;
		}
	}
	return 0;
}

// copy only the field, without ;, but will keep spaces
char* mgetvalue(char **str) {
	char *p;
	unsigned int i=0;

	if (!str) return NULL;
	for(;;) {
		if (!(*str)[i] || (*str)[i] == ';' || (*str)[i] == '\t') { // end of string/field
			p = malloc(i+1);
			if (!p) { return NULL; }
			strncpy(p, *str, i);
			p[i] = 0;
			if (!(*str)[i]) { *str = &(*str)[i]; } // end of string
			else { *str = &(*str)[i+1]; } // next value
			return p;
		}
		i++;
	}
}

// move a given pointer to the next value in buffer
int skip(char **p) {
	if (!p) { return -1; } // NULL pointer
	if (!*p) { return -1; } // NULL pointer
	while (**p && **p != ';' && **p != '\t') {
		(*p)++;
	}
	if (!**p) { return 1; } //end of string
	(*p)++;
	return 0;
}

int serial_init(const char *device) {
	struct termios options;
	int fd = -1;
	fd = open(device, O_RDWR | O_NOCTTY | O_NDELAY);
	if((fd == -1)||(!fd)) {
		fd = 0;
		return 0;
	}
	fcntl(fd, F_SETFL, 0); // blocking mode

	tcgetattr(fd, &options);
	options.c_oflag = 0;
	options.c_lflag = 0;
	options.c_iflag = 0;
	options.c_cflag = 0;

	cfsetospeed(&options, B4800);
	cfsetispeed(&options, B4800);

	options.c_cflag |= (CLOCAL | CREAD);
	options.c_cflag &= ~CSTOPB; // 1 bit stop
	options.c_cflag &= ~CSIZE; // mask the character size bits
	options.c_cflag |= CS8; // 8 bits

	options.c_cflag &= (~PARENB & ~PARODD); // no parity
	options.c_cc[VMIN] = 1;
	options.c_cc[VTIME] = 0;

	options.c_oflag &= ~OPOST; // raw mode for output

	options.c_iflag |= IGNBRK; // ignore breaks
	options.c_iflag &= ~(IXON | IXOFF | IXANY); // no software flow control
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // raw mode for input

	tcsetattr(fd, TCSANOW, &options);
	return fd;
}

float float_diff_abs(float a, float b) {
	if (a>b) { return a-b; }
	return b-a;
}

typedef struct {
	unsigned int enabled;
	char *group;
	char *name;
	char *gas;
	char *unit;
	float worst_value;
	float current_value;
	time_t worst_value_time;
	unsigned int current_alarm;
	int sensor_failure;
} tchannels;
tchannels *channels;
unsigned int maxchannels;

const char* alarms[16] = {
	"A1 UN-ACK",
	"A1 ACTIVE",
	"A1 TRIPPED",
	"CRITICAL FAULT",
	"A2 UN-ACK",
	"A2 ACTIVE",
	"A2 TRIPPED",
	"HCAL",
	"A3 UN-ACK",
	"A3 ACTIVE",
	"A3 TRIPPED",
	"INHIBIT",
	"UN-ACK",
	"ACTIVE",
	"TRIPPED",
	"NORESPONSE"
};

const char *output_directory = NULL;
int update_channel_value(unsigned int channel, float value) {
	FILE *fd;
	char filename[FILENAME_BUFFER_SIZE+1];
	time_t lt;

	lt = time(NULL);

	if ((channel > 0) && (channel <= maxchannels)) {
		if (!strcmp(channels[channel-1].gas, "O2")) { // oxygen ?
			if (float_diff_abs(value, 21.0) > float_diff_abs(channels[channel-1].worst_value, 21.0)) { // new "worst value" for dioxygen
				channels[channel-1].worst_value_time = lt;
				channels[channel-1].worst_value = value;
			}
		}
		else { // not oxygen ?
			if (value > channels[channel-1].worst_value) { // new "worst value", other gases
				channels[channel-1].worst_value_time = lt;
				channels[channel-1].worst_value = value;
			}
		}
		if (lt>(channels[channel-1].worst_value_time+300)) { // 5' timeout
			channels[channel-1].worst_value_time = lt;
			channels[channel-1].worst_value = value;
		}
	}

	snprintf(filename, FILENAME_BUFFER_SIZE, "%s/%05u.value", output_directory, channel+file_number_offset);
	fd = fopen(filename, "w");
	if (!fd) {
		printlog(errno, "Can't write to", filename);
		return -1;
	}
	fprintf(fd, "%.1f\n", value);
	if ((channel > 0) && (channel <= maxchannels)) {
		fprintf(fd, "%.1f\n", channels[channel-1].worst_value);
		channels[channel-1].current_value = value;
	}
	fclose(fd);
	return 0;
}

int update_channel_alarm(unsigned int channel, unsigned int value) {
	FILE *fd;
	char filename[FILENAME_BUFFER_SIZE+1];
	char tmp[2000], alarm_str[2000];
	unsigned int b, v = 1;
	int new_inhibit_flag = 0, new_alarm_flags = 0;

	if ((channel > 0) && (channel <= maxchannels)) {
		if (value != channels[channel-1].current_alarm) {
			v=1;
			alarm_str[0] = '\0';
			for (b=0; b<16; b++) {
				if ((value & v)&&!(channels[channel-1].current_alarm & v)) {
					snprintf(tmp, sizeof tmp, "+[%s] ", alarms[b]); strncat(alarm_str, tmp, sizeof alarm_str);
					if (b == 11)     { new_inhibit_flag = 1; }
					else if (b != 7) { new_alarm_flags = 1;  }
				}
				if (!(value & v)&&(channels[channel-1].current_alarm & v)) {
					snprintf(tmp, sizeof tmp, "-[%s] ", alarms[b]); strncat(alarm_str, tmp, sizeof alarm_str);
				}
				v*=2;
			}
			snprintf(tmp, sizeof tmp, "Ch. %u (%s) status changed:", channel, channels[channel-1].name);
			if (!channels[channel-1].sensor_failure) {
				printlog(0, tmp, alarm_str);
			}
			channels[channel-1].current_alarm = value;
		}

		// if sensor is nominal (no alarm or anything)
		if (value == 0) {
			if (channels[channel-1].sensor_failure) {
				snprintf(tmp, sizeof tmp, "Ch. %u (%s) is no longer defective", channel, channels[channel-1].name);
				printlog(0, tmp, NULL);
			}
			channels[channel-1].sensor_failure = 0;
		}

		// if INHIBIT and an alarm bit flags are sets at the same time, it means a sensor broke.
		else if (new_inhibit_flag && new_alarm_flags) {
			if (!channels[channel-1].sensor_failure) {
				snprintf(tmp, sizeof tmp, "Ch. %u (%s) is now marked as DEFECTIVE", channel, channels[channel-1].name);
				printlog(0, tmp, NULL);
			}
			channels[channel-1].sensor_failure = 1;
		}
	}

	snprintf(filename, FILENAME_BUFFER_SIZE, "%s/%05u.alarm", output_directory, channel+file_number_offset);
	fd = fopen(filename, "w");
	if (!fd) {
		printlog(errno, "Can't write to", filename);
		return -1;
	}
	fprintf(fd, "%u\n%d\n", value, channels[channel-1].sensor_failure);
	fclose(fd);
	return 0;
}

int read_channels_infos_file(char *conf_file) {
	FILE *fd, *fo;
	char buf[READCONFIG_LINE_BUFSIZE+1], *ptr;
	char filename[FILENAME_BUFFER_SIZE+1];
	unsigned int n = 0, channel = 0;

	maxchannels=0;
	fd = fopen(conf_file, "r");
	if (!fd) {
		printlog(errno, "Can't open the channels informations file:", conf_file);
		return -1;
	}

	// first, detect the maximum numbers of channels
	while (fgets(buf, sizeof(buf), fd)) { // end of file ?
		if ((buf[0] == '\r') || (buf[0] == '\n')) { continue; } // empty line ?
		buf[READCONFIG_LINE_BUFSIZE] = 0;
		n = atoi(buf);
		if (n > maxchannels) { maxchannels = n; }
	}
	{
		char tmp[1000];
		snprintf(tmp, sizeof tmp, "Allocating memory for the %u channel(s) I saw in %s... ", maxchannels, conf_file);
		printlog(0, tmp, NULL);

		channels = malloc(maxchannels * sizeof(tchannels));
		if (!channels) {
			snprintf(tmp, sizeof tmp, "Allocation failed! (%u bytes of structure array)", (unsigned int)(maxchannels * sizeof(tchannels)));
			printlog(0, tmp, NULL);
			return -1;
		}
		memset(channels, 0, maxchannels * sizeof(tchannels));
	}

	// then taking the data from it !
	fseek(fd, 0, SEEK_SET);
	while (fgets(buf, sizeof(buf), fd)) { // not the end of file ?
		if ((buf[0] == '\r') || (buf[0] == '\n')) { continue; } // empty line ?
		buf[READCONFIG_LINE_BUFSIZE] = 0;
		ptr = buf;
		channel = atoi(ptr); skip(&ptr);
		if ((!channel) || (channel > maxchannels)) { continue; }

		channels[channel-1].enabled = atoi(ptr); skip(&ptr);
		channels[channel-1].group   = mgetvalue(&ptr);
		channels[channel-1].name    = mgetvalue(&ptr);
		channels[channel-1].gas     = mgetvalue(&ptr);
		channels[channel-1].unit    = mgetvalue(&ptr);

		if (!channels[channel-1].enabled) { continue; }
		snprintf(filename, FILENAME_BUFFER_SIZE, "%s/%05u.infos", output_directory, channel+file_number_offset);
		fo = fopen(filename, "w");
		if (!fo) {
			printlog(errno, "Can't write to", filename);
			fclose(fd);
			return -1;
		}
		fprintf(fo, "%u\n", channels[channel-1].enabled);
		fprintf(fo, "%s\n", channels[channel-1].group);
		fprintf(fo, "%s\n", channels[channel-1].name);
		fprintf(fo, "%s\n", channels[channel-1].gas);
		fprintf(fo, "%s\n", channels[channel-1].unit);

		fclose(fo);
	}

	printlog(0, "Writing channels informations OK!", NULL);
	fclose(fd);
	return 0;
}

typedef struct {
	unsigned char slave_address;
	unsigned char function_code;
	unsigned int req_register_start;
	unsigned int req_register_count;
	unsigned int response_byte_count;
	unsigned int exception;
} tquery;

void proceed_modbus_response(tquery msg, unsigned char *data) {
	unsigned int i;
	float value = NAN;
	unsigned int uvalue = 0x010000;

	if (msg.function_code == 0x04) { // read input registers, data is 16 bits each
		// response to Read Input Register
		for (i=0; i<msg.req_register_count; i++) {
			if (msg.exception) { value = NAN; }
			else if (i*2>msg.response_byte_count) { value = NAN; }
			else {
				uvalue = ((((unsigned int)data[0+i])*256 + (unsigned int)data[1+i]));
				if (uvalue&0x8000) { value=(float)(uvalue&0x7FFF)-0x8000; }
				else { value = (float)(uvalue&0x7FFF); }
			}

			if (msg.req_register_start+i>=1000) {
				// alarm bits ?
				if (!daemon_mode) {
					printf("Ch.%3u:", msg.req_register_start+i-1000+1);
				}
				update_channel_alarm(msg.req_register_start+i-1000+1, uvalue);
				if (!daemon_mode) {
					if (msg.req_register_start+i-1000 < maxchannels && channels[msg.req_register_start+i-1000].name) {
						printf(" %-30s", channels[msg.req_register_start+i-1000].name);
					}
					if (!uvalue) { printf(" [OK]"); }
					else {{
						unsigned int b, v = 1;
						printf("\a");
						for (b=0; b<16; b++) {
							if (uvalue & v) { printf(" [%s]", alarms[b]); }
							v*=2;
						}
					}}
				}
			}
			else { // value ?
				if (msg.req_register_start+i < maxchannels && channels[msg.req_register_start+i].gas) {
					if (!strcmp(channels[msg.req_register_start+i].gas, "O2")) {
						// if the gas is oxigen, we need to divide the value by 10.
						value/=10;
					}
					else if (!strcmp(channels[msg.req_register_start+i].gas, "H2S")) {
						// if the gas is sulfur dihydrogen, we need to divide the value by 10.
						value/=10;
					}

				}
				if (!daemon_mode) {
					printf("Ch.%3u:", msg.req_register_start+i+1);
					if (msg.req_register_start+i < maxchannels && channels[msg.req_register_start+i].name) { printf(" %-30s", channels[msg.req_register_start+i].name); }
					printf("%8.1f", value);
					if (msg.req_register_start+i < maxchannels && channels[msg.req_register_start+i].unit) { printf(" %s", channels[msg.req_register_start+i].unit); }
				}
				update_channel_value(msg.req_register_start+i+1, value);
			}
		}
	}
	else {{
		char tmp[1000];
		snprintf(tmp, sizeof tmp, "Function code %u is unknow to proceed!", msg.function_code);
		printlog(0, tmp, NULL);
	}}
}

typedef enum {
	UNKNOW = 0,
	QUERY_ADDRESS,
	QUERY_FUNCTION_CODE,
	QUERY_RIR_REG_START_HIGH,
	QUERY_RIR_REG_START_LOW,
	QUERY_RIR_REG_COUNT_HIGH,
	QUERY_RIR_REG_COUNT_LOW,
	QUERY_CRC_HIGH,
	QUERY_CRC_LOW,
	RESPONSE_ADDRESS,
	RESPONSE_ERROR_CODE,
	RESPONSE_FUNCTION_CODE,
	RESPONSE_BYTE_COUNT,
	RESPONSE_DATA,
	RESPONSE_CRC_HIGH,
	RESPONSE_CRC_LOW
} tstate;

int main(int argc, char **argv) {
	int fd = 0;
	int n = 0, c = 0, arg;
	char *serial_device=NULL, *channels_file=NULL;
	int car[2]={0,0};
	unsigned char buf[100];
	tstate state = UNKNOW;
	tquery query;
	struct timeval time;
	long int old_usec, delay_usec;
	ssize_t ret;
	struct stat sb;

	for(n=1, arg=0; n<argc; n++) {
		if (argv[n][0] == '-') {
			if (!strcmp(argv[n], "-d")) {
				daemon_mode = 1;
			}
			else if (!strcmp(argv[n], "-n")) {
				file_number_offset = atoi(argv[++n]);
				if (file_number_offset<=0) {
					fprintf(stderr, "%s: invalid offset\n", argv[n]);
					print_usage();
					return -1;
				}
			}
			else {
				fprintf(stderr, "%s: unknow option\n", argv[n]);
				print_usage();
				return -1;
			}
		}
		else {
			switch(++arg) {
				case 1:
					serial_device = argv[n];
					break;

				case 2:
					channels_file = argv[n];
					break;

				case 3:
					logfile = argv[n];
					break;

				case 4:
					output_directory = argv[n];
					break;

				default:
					fprintf(stderr, "%s: too many arguments\n", argv[n]);
					print_usage();
					return -1;
			}

		}
	}

	if (!output_directory) {
		output_directory = DEFAULT_OUTPUT_DIR;
	}

	if (!logfile) {
		logfile = DEFAULT_LOGFILE;
	}

	if (!serial_device || !channels_file) {
		print_usage();
		return -1;
	}

	printf("*** Log file: %s ***\n", logfile);

	if (printlog(0, "Program starting...", NULL)) {
		// we'll check the logfile writing only at startup,
		// because program must not crash...
		return -1;
	}

	/* creating the output directory */
	n = stat(output_directory, &sb);
	if (n || !S_ISDIR(sb.st_mode)) {
		if (!mkdir(output_directory, 0755)) {
			printlog(0, "Created", output_directory);
		}
		else {
			printlog(errno, "Can't create", output_directory);
			return -1;
		}
	}

	/* reading the channels informations file you can find on Draeger equipment */
	if (read_channels_infos_file(channels_file)) {
		printlog(0, "Exiting...", NULL);
		return -1;
	}

	/* this should not fail but i check it anyway */
	if(gettimeofday(&time, NULL) != 0) {
		perror("gettimeofday() failed !");
		printlog(0, "Exiting...", NULL);
		return -1;
	}
	old_usec=time.tv_usec;

	/* opening the serial port */
	fd = serial_init(serial_device);
	if (!fd) {
		printlog(errno, "Unable to open the serial port", serial_device);
		printlog(0, "Exiting...", NULL);
		return -1;
	}

	n=0;
	for(;;) {
		ret = read(fd, car, 1);

		if (ret==0) { // EOF
			if (!daemon_mode) { printf("\n"); fflush(stdout); }
			printlog(errno, "read() returned \"End of File\"", NULL);
			printlog(0, "Exiting...", NULL);
			return -255;
		}
		else if (ret<0) { // error
			if (!daemon_mode) { printf("\n"); fflush(stdout); }
			printlog(errno, "read() failed: ", NULL);
			printlog(0, "Exiting...", NULL);
			return -254;
		}

		gettimeofday(&time, NULL);
		delay_usec = time.tv_usec - old_usec; old_usec=time.tv_usec;
		if (delay_usec <0) { delay_usec+=1000000; }

		if (delay_usec>80000) {
			n=0; state=QUERY_ADDRESS;
			if (!daemon_mode) { printf("\n"); fflush(stdout); }
		}
		if (!daemon_mode) { printf("%02x", car[0]&0xFF); }

		if (n>100) { n=100; }
		buf[n++] = car[0]&0xFF;

		switch(state) {
			case QUERY_ADDRESS:
				query.slave_address = (unsigned char)car[0]&255;
				state = QUERY_FUNCTION_CODE;
				break;

			case QUERY_FUNCTION_CODE:
				query.function_code = (unsigned char)car[0]&255;
				if (query.function_code == 0x04) { state = QUERY_RIR_REG_START_HIGH; }
				else {{
					char tmp[1000];
					snprintf(tmp, sizeof tmp, "Function code %u is unknow, ignored frame", query.function_code);
					printlog(0, tmp, NULL);
					state = UNKNOW;
				}}
				break;

			case QUERY_RIR_REG_START_HIGH:
				query.req_register_start = ((unsigned int)car[0]&255)<<8;
				state = QUERY_RIR_REG_START_LOW;
				break;

			case QUERY_RIR_REG_START_LOW:
				query.req_register_start += ((unsigned int)car[0]&255);
				state = QUERY_RIR_REG_COUNT_HIGH;
				break;

			case QUERY_RIR_REG_COUNT_HIGH:
				query.req_register_count = ((unsigned int)car[0]&255)<<8;
				state = QUERY_RIR_REG_COUNT_LOW;
				break;

			case QUERY_RIR_REG_COUNT_LOW:
				query.req_register_count += ((unsigned int)car[0]&255);
				state = QUERY_CRC_HIGH;
				break;

			case QUERY_CRC_HIGH:
				state = QUERY_CRC_LOW;
				break;

			case QUERY_CRC_LOW:
				if (!crc16(buf, n)) {
					n=0;
					if (!daemon_mode) { printf(" [CRC OK] "); }
					state = RESPONSE_ADDRESS;
				}
				else {
					if (!daemon_mode) { printf(" [BAD CRC] "); }
					printlog(0, "Bad CRC in query!", NULL);
					state = UNKNOW;
				}
				break;

			case RESPONSE_ADDRESS:
				if (((unsigned char)car[0]&255) != query.slave_address) {{
					char tmp[1000];
					snprintf(tmp, sizeof tmp, "Slave adress in query (%u) don't match the response (%u), ignored frame", query.slave_address, car[0]);
					printlog(0, tmp, NULL);
				}}
				state = RESPONSE_FUNCTION_CODE;
				break;

			case RESPONSE_FUNCTION_CODE:
				query.response_byte_count = 0;
				if (((unsigned char)car[0]&255) == query.function_code) { // ok
					query.exception = 0;
					state = RESPONSE_BYTE_COUNT;
				}
				else if (((unsigned char)car[0]&255) == (query.function_code|0x80)) { // exception
					query.exception = 0xFF;
					state = RESPONSE_ERROR_CODE;
				}
				else {{ // huh ?
					char tmp[1000];
					query.exception = 0xFF;
					snprintf(tmp, sizeof tmp, "Function code in query (%u) don't match the response (%u), ignored frame", query.function_code, car[0]);
					printlog(0, tmp, NULL);
					state = UNKNOW;
				}}
				break;

			case RESPONSE_ERROR_CODE:
				query.exception = ((unsigned int)car[0]&255);
				state = RESPONSE_CRC_HIGH;
				break;

			case RESPONSE_BYTE_COUNT:
				query.response_byte_count = ((unsigned int)car[0]&255);
				if (query.response_byte_count) {
					c = query.response_byte_count;
					state = RESPONSE_DATA;
				}
				else {
					state = UNKNOW;
				}
				break;

			case RESPONSE_DATA:
				c--; // we get the data in buf[] meanwhile !
				if (!c) {
					state = RESPONSE_CRC_HIGH;
				}
				break;

			case RESPONSE_CRC_HIGH:
				state = RESPONSE_CRC_LOW;
				break;

			case RESPONSE_CRC_LOW:
				if (!crc16(buf, n)) {
					n=0;
					if (!daemon_mode) { printf(" [CRC OK] "); }
					state = QUERY_ADDRESS;
					proceed_modbus_response(query, buf+3);
				}
				else {
					if (!daemon_mode) { printf(" [BAD CRC] "); }
					printlog(0, "Bad CRC in response!", NULL);
					state = UNKNOW;
				}
				break;

			default:
			case UNKNOW:
				break;
		}
	}
	printlog(0, "Closing the serial port...", NULL);
	close(fd);
	printlog(0, "Exiting...", NULL);
	return 0;
}
