/*
	Project:   gm_ (Munin plugin part of RegarTercept)
	Author:    RJS <romain.jooris@univ-lille1.fr>
	Copyright: UCCS/CNRS <http://uccs.univ-lille1.fr/>
	Warning:   This code is not, and must not be, commercial.
	Date:      February 2013 (first development)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/time.h>
#include "config.h"

#define BINNAME "gm_"
#define BUFSIZE  200
//#define ENABLE_ALARM_IN_MUNIN

// from "ME_REGARD_carte_modbus_f_00.pdf"
#define A1_UN_ACKNOWLEDGED   1
#define A1_ACTIVE            2
#define A1_TRIPPED           4
#define CRITICAL_FAULT       8
#define A2_UN_ACKNOWLEDGED  16
#define A2_ACTIVE           32
#define A2_TRIPPED          64
#define HCAL               128
#define A3_UN_ACKNOWLEDGED 256
#define A3_ACTIVE          512
#define A3_TRIPPED        1024
#define INHIBIT           2048
#define UN_ACKNOWLEDGED   4096
#define ACTIVE            8192
#define TRIPPED          16384
#define CHANNEL_TIMEOUT  32768

FILE* openfile(const char *name, const char *ext, time_t *currenttime) {
	char filename[FILENAME_BUFFER_SIZE];
	FILE *fd;
	struct stat attributes;

	strcpy(filename, DEFAULT_OUTPUT_DIR);
	strcat(filename, "/");
	strcat(filename, name);
	if (ext) {
		strcat(filename, ".");
		strcat(filename, ext);
	}

	if (currenttime != NULL) {
		if (stat(filename, &attributes)){
			perror(filename);
			return NULL;
		}
		if (*currenttime - attributes.st_mtime > 300) {
			fprintf(stderr, "%s is outdated (%li\"). We don't want to graph that !\n", filename, (signed long int)(*currenttime - attributes.st_mtime));
			return NULL;
		}
	}

	fd = fopen(filename, "r");
	if (!fd) { perror(filename); }
	return fd;
}

int main(int argc, char *argv[]) {
	char *sensor;
	char buf[BUFSIZE+1];
	char buf2[BUFSIZE+1];
	FILE *fd;
#ifdef ENABLE_ALARM_IN_MUNIN
	int al = -1;
#endif
	time_t currenttime;

	if(time(&currenttime) == -1) {
		perror("time() failed !");
		return -1;
	}

	sensor = strstr(argv[0], BINNAME);
	if (sensor) { sensor+=strlen(BINNAME); }
	if (!(sensor && *sensor)) {
		fprintf(stderr, "Please use symbolics links !\n");
		return -1;
	}

	if (argv[1] && strcmp(argv[1], "autoconf") == 0) {
		printf("yes\n");
		return 0;
	}
	if (argv[1] && strcmp(argv[1], "config") == 0) {
		fd = openfile(sensor, "infos", NULL);
		if(!fd) { return -1; }

		// infos graph
		printf("graph_category gasesmonitoring\n");
		fgets(buf, sizeof(buf), fd); //enabled ("1")
		fgets(buf, sizeof(buf), fd); //group ("")
		fgets(buf, sizeof(buf), fd); //name ("Labo A - SAS - capteur O2")
		printf("graph_info %s", buf);
		printf("graph_title %s", buf);
		printf("graph_scale no\n");

		// infos value
		fgets(buf, sizeof(buf), fd); //gas ("O2")
		printf("sensor.label %s", buf);
		fgets(buf, sizeof(buf), fd); //unit ("%VOL")
		if (buf[0] != 0x0d && buf[0] != 0x0a) { printf("graph_vlabel %s", buf); }
		else { printf("graph_vlabel (value)\n"); }
		printf("sensor.type GAUGE\n");

		// infos alarm
#ifdef ENABLE_ALARM_IN_MUNIN
		printf("alarm.label Alarm status\n");
		printf("alarm.graph no\n");
		printf("alarm.warning 0.5\n");
		printf("alarm.critical 1.5\n");
#endif

		fclose(fd);
		return 0;
	}
	fd = openfile(sensor, "value", &currenttime);
	if(!fd) {
#ifdef ENABLE_ALARM_IN_MUNIN
		printf("alarm.value -1\n"); // error
#endif
		return -1;
	}
	fgets(buf,  sizeof(buf),  fd); // current value
	fgets(buf2, sizeof(buf2), fd); // 5' worst value
	if (!buf2[0] || !buf2[1] || buf2[1] == '\n') {
		fprintf(stderr, "Worst value (5') unavailable !\n");
		printf("sensor.value %s", buf);
	}
	else {
		printf("sensor.value %s", buf2);
	}
	fclose(fd);

#ifdef ENABLE_ALARM_IN_MUNIN
	fd = openfile(sensor, "alarm", &currenttime);
	if(!fd) {
		return -1;
		printf("alarm.value -1\n"); // no alarm status
	}
	fgets(buf, sizeof(buf), fd); // alarm status binary coded (like "0", "26624", or "30720")
	fgets(buf2, sizeof(buf2), fd); // defective sensor
	if (atoi(buf2)) {
		printf("alarm.value -1\n"); // sensor is defective
	}
	al = atoi(buf);
	if (!al) {
		printf("alarm.value 0\n"); // ok
	}
	else if (al & (CRITICAL_FAULT|HCAL|INHIBIT|CHANNEL_TIMEOUT)) {
		printf("alarm.value -1\n"); // channel not available
	}
	else if (al & (A2_ACTIVE|A2_UN_ACKNOWLEDGED|A3_ACTIVE|A3_UN_ACKNOWLEDGED)) {
		printf("alarm.value 2\n"); // critical value
	}
	else if (al & (A1_ACTIVE|A1_UN_ACKNOWLEDGED|ACTIVE|UN_ACKNOWLEDGED)) {
		printf("alarm.value 1\n"); // warning value
	}
	else {
		printf("alarm.value 0\n"); // ...ok
	}
#endif
	return 0;
}
